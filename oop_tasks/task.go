package main

import (
	"errors"
	"fmt"
	"time"
)
var steps = map[string]int {"analyzing":0,"designing":0,"coding":0,"debugging":0,"testing":0}
func main(){
	//var newTask = Task{Name: "Art.uz",Status: "Created",developingStep: steps}
}

var projects[] *Task
type Task struct {
	ID int
	Name, Status   string
	developingStep map[string]int

}
type Director struct {
	
}


func (director *Director)GiveDevTask(newTask *Task, teamLead *TeamLead) (int,error) {
	id := len(projects) + 1
	for _, val := range projects{
		if val.Name == newTask.Name{
			return 0, errors.New("already exists")
		}
	}
	if teamLead.task != nil{
		return 0, errors.New("TeamLead has task")
	}
	newTask.ID = id
	projects = append(projects, newTask)
	teamLead.task = newTask
	return id, nil

}

type TeamLead struct {
	task *Task
}

func (tLead *TeamLead)DelegateDevTask(step string, programmer *Programmer)(string, error)  {
	
}
func (tLead *TeamLead)CheckTask()  {

}
type Programmer struct {
	
}



func (programmer *Programmer)Develop(taskStep string, task *Task)  error  {
	k := 0
	i := 0
	for key, val := range task.developingStep{
		i++
		if key == taskStep{
			if i-1 != k{
				msg := fmt.Sprintf("Not ready for '%v'\n", taskStep)
				return errors.New(msg)
			}
		}
		if val == 1{
			k++
		}
	}
	task.developingStep[taskStep] = 1
	fmt.Println("Developing:", task.Name)
	time.Sleep(time.Second * 100)
	return nil
}
