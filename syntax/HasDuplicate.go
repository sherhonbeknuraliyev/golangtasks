package main

import "fmt"

func main()  {
	var arr = []int{1,2,34,43,42312,12,342,0}
	fmt.Println(HasDuplicateElement(arr))


}

func HasDuplicateElement(arr[] int) bool {
	for i, val := range arr{
		for _, val2 := range arr[i+1:]{
			if val == val2{
				return true
			}
		}
	}
	return false

}