package main

import "fmt"

func main() {
	fmt.Printf("N=")
	var N int
	fmt.Scan(&N)
	for i := 1; i <= N; i++ {
		fmt.Printf("%d", i)
		if i%3 == 0 && i%5 == 0 {
			fmt.Printf(" FizzBuzz")
		} else if i%3 == 0 {
			fmt.Printf(" Fizz")
		} else if i%5 == 0 {
			fmt.Printf(" Buzz")
		} else {
			fmt.Printf("  %d", i)
		}
		fmt.Printf("\n")
	}
}
