package main

import (
	"fmt"
	"log"
)

func main() {
	var num int

	fmt.Printf("N=")
	_, err := fmt.Scan(&num)
	if err != nil {
		log.Fatal(err)
	}
	var f1, f2, i int = 0, 1, 0

	for i < num {
		fmt.Print(f1, " ")
		t := f2
		f2 = f2 + f1
		f1 = t
		i++
	}

}
