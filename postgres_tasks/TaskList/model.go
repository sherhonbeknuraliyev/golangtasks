package main

import (
	"database/sql"
	"fmt"
)

type Task struct {
	ID int
	Name, Status, Priority,CreatedBy string
	CreatedAt, DueDate string
}
type TaskManager struct {
	db *sql.DB
}

func (taskManager TaskManager)AddTask(id,newTask Task) (int, error) {
	msg := fmt.Sprintf(`INSERT INTO tasks (name, status, priority, createdby,createdat,duedate) VALUES ($1,$2,$3,$4,$5,$6);`)
	_, err := taskManager.db.Query(msg, newTask.Name, newTask.Status, newTask.Priority, newTask.CreatedBy, newTask.CreatedAt, newTask.DueDate)
	if err != nil {
		return 0, err
	}
	task , err:= taskManager.GetByName(newTask.Name)
	if err != nil {
		return 0,err
	}
	return task.ID, err
}
func (taskManager TaskManager)GetAll()([]*Task, error) {
	rows, err:= taskManager.db.Query("SELECT * FROM tasks;")
	if err != nil{
		return nil, err
	}
	var tasks[] *Task
	for rows.Next(){
		var ID int
		var Name,Status,Priority, CreatedBy string
		var CreatedAt, DueAt string
		err := rows.Scan(&ID, &Name,&Status,&Priority, &CreatedBy,&CreatedAt,&DueAt)
		if err != nil{
			return nil, err
		}
		task := Task{ID: ID,Name: Name,Status: Status,Priority: Priority,CreatedBy: CreatedBy,CreatedAt: CreatedAt,DueDate: DueAt}
		tasks = append(tasks,&task)
	}
	return tasks, nil
}
func (taskManager TaskManager)GetByName(name string) (*Task,error ){
	msg := fmt.Sprintf(`SELECT * FROM tasks WHERE name=$1;`)
	rows , err := taskManager.db.Query(msg,name)

	var ID int
	var Name,Status,Priority, CreatedBy string
	var CreatedAt, DueAt string
	err = rows.Scan(&ID, &Name, &Status, &Priority, &CreatedBy, &CreatedAt, &DueAt)
	if err != nil{
		return nil, err
	}

	return &Task{ID,Name,Status,Priority,CreatedBy,CreatedAt,DueAt}, nil
}
func (taskManager TaskManager)GetById(id string) (*Task, error) {
	msg := fmt.Sprintf("SELECT * FROM tasks WHERE id=$1;")
	rows , err := taskManager.db.Query(msg,id)
	var ID int
	var Name,Status,Priority, CreatedBy string
	var CreatedAt, DueAt string
	err = rows.Scan(&ID, &Name, &Status, &Priority, &CreatedBy, &CreatedAt, &DueAt)
	if err != nil{
		return nil, err
	}
	return &Task{ID,Name,Status,Priority,CreatedBy,CreatedAt,DueAt}, nil
}
func (taskManager TaskManager)UpdateTask(ID string,newTask Task) (*Task,error) {
	msg := fmt.Sprintf(`UPDATE tasks SET name=$1, status=$2, priority=$3,createdby=$4,createdat=$5,duedate=$6 WHERE id=$7;`)
	_, err := taskManager.db.Query(msg, newTask.Name, newTask.Status,newTask.Priority, newTask.CreatedBy, newTask.CreatedAt, newTask.DueDate, ID)
	if err != nil {
		return nil, err
	}
	task, err :=  taskManager.GetById(ID)
	if err != nil{
		return nil, err
	}
	return task, nil
}
func (taskManager TaskManager)CreateTable() error {
	msg := fmt.Sprintf("CREATE TABLE tasks (id SERIAL PRIMARY KEY,name VARCHAR(200) UNIQUE NOT NULL, status VARCHAR(200) NOT NULL , priorityVARCHAR(200) NOT NULL , createdby VARCHAR(200) NOT NULL,createdat DATE NOT NULL,duedate DATE NOT NULL);")
	_, err := taskManager.db.Exec(msg)
	if err != nil {
		return err
	}
	return nil
}