package main

import (
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

type Contact struct {
	FirstName , LastName, Phone, Email, Position string
	ID int
}

func save(newContact *Contact) *Contact {
	msg := fmt.Sprintf(
		"INSERT INTO contacts (firstName, lastName , phone, email,position) VALUES ($1,$2,$3,$4,$5) RETURNING id",
		)

	rows, err := db.Query(msg, newContact.FirstName, newContact.LastName, newContact.Phone, newContact.Email,newContact.Position)
	if err != nil {
		log.Fatalln(err)
	}

	if rows.Next(){
		var id int
		err := rows.Scan(&id)
		if err != nil {
			log.Fatalln(err)
		}
		newContact.ID = id
	}

	return newContact
}