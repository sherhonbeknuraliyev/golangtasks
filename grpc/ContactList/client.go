package main

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
	"lion.com/contact_list/contact/pb"
	"log"
)

func main()  {
	var conn *grpc.ClientConn
	conn,err := grpc.Dial(":9000", grpc.WithInsecure())
	if err!= nil{
		log.Fatalln(err)
	}
	s := pb.NewCreateContactServiceClient(conn)

	res, err := s.CreateContact(context.Background(), &pb.NewContact{FirstName: "Jofn",LastName: "Lion",Position: "fwefdewd",Phone: "+432432",Email: "test1@grpc.com"})
	if err != nil{
		log.Fatalln(err)
	}
	fmt.Println("Created contact:" ,res)
	
}
