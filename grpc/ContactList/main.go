package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"google.golang.org/grpc"
	_ "google.golang.org/protobuf/proto"
	"lion.com/contact_list/contact/pb"
	"log"
	"net"
)

var db, _ = connectDB()



func main()  {
	lis, err := net.Listen("tcp", ":9000")
	if err != nil{
		log.Fatalln(err)
	}
	grpcServer := grpc.NewServer()
	s := Server{}
	pb.RegisterCreateContactServiceServer(grpcServer, &s)
	if err := grpcServer.Serve(lis); err != nil{
		log.Fatalln(err)
	}
}

type Server struct {

}

func (s *Server)CreateContact(ctx context.Context,req *pb.NewContact) (*pb.Contact, error) {
	newContact := Contact{FirstName: req.GetFirstName(), LastName: req.GetLastName(),Phone: req.GetPhone(),Email: req.GetEmail(),Position: req.GetPosition()}
	save(&newContact)
	return &pb.Contact{ID: int32(newContact.ID),FirstName: newContact.FirstName,LastName: newContact.LastName,Phone: newContact.Phone,Email: newContact.Email,Position: newContact.Position}, nil

}


func connectDB() (*sql.DB, error) {
	const (
		host = "localhost"
		port = 5432
		user = "postgres"
		password = "root"
		dbname = "golangtestdb"
	)
	dataSource := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s sslmode=disable password=%s", host, port, user,dbname, password )
	db_, err := sql.Open("postgres",dataSource)
	if err != nil{
		log.Fatal(err)
	}
	if err = db_.Ping(); err != nil {
		log.Fatal(err)
	}
	return db_, nil
}
