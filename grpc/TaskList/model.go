package main

import (
	"database/sql"
	"fmt"
	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"lion.com/task_list/pb"
	"time"
)

type Task struct {
	Name, Status,Priority,CreatedBy string
	CreatedAt, DueDate time.Time
	ID int32
}
type TaskManger struct {
	db *sql.DB
}


func (taskManager *TaskManger)Create(newTask *pb.Task)(*pb.Task,error)  {
	msg := fmt.Sprint(`INSERT INTO tasks (name, status, priority, createdby,createdat,duedate) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *;`)
	rows, err := taskManager.db.Query(msg, newTask.GetName(), newTask.GetStatus(), newTask.GetPriority(), newTask.GetCreatedBy(), newTask.GetCreatedAt().AsTime(), newTask.GetDueDate().AsTime())
	if err!=nil{
		fmt.Println(err)
		return nil, err
	}
	fmt.Println("Rows", rows)
	scanTasks, err := taskManager.scanTasks(rows)
	if err != nil {
		return nil, err
	}
	fmt.Println(scanTasks)

	if err != nil {
		return nil, err
	}

	return scanTasks[0], err

}
func (taskManager TaskManger)GetById(id int32)(*pb.Task, error)  {
	msg := fmt.Sprintf("SELECT * FROM tasks WHERE id=%v;", id)
	rows ,err:= taskManager.db.Query(msg)
	if err != nil{
		return nil, err
	}
	tasks, err := taskManager.scanTasks(rows)
	if err != nil {
		return nil, err
	}
	if len(tasks) == 0{
		return nil, status.Error(codes.NotFound,"Not found")
	}
	return tasks[0], nil

}
func (taskManager TaskManger)GetAll() ([]*pb.Task, error) {
	msg := fmt.Sprintf("SELECT * FROM tasks;")
	rows, err := taskManager.db.Query(msg)
	tasks, err := taskManager.scanTasks(rows)
	if err != nil {
		return nil, err
	}
	return tasks,nil
}
func (taskManager *TaskManger)Delete(task *pb.Task)(bool, error)  {
	msg := fmt.Sprintf("DELETE FROM tasks WHERE id=%v;", task.Id)
	_, err := taskManager.db.Query(msg)
	if err != nil{
		return false, status.Error(codes.Unknown, err.Error())
	}
	return true, nil
}
func (taskManager *TaskManger)Update(newTask *pb.Task, id int32) (*pb.Task, error) {
	msg := fmt.Sprintf(`UPDATE tasks SET name=$1, status=$2, priority=$3,createdby=$4,createdat=$5,duedate=$6 WHERE id=$7 RETURNING *;`)
	rows, err := taskManager.db.Query(msg, newTask.Name, newTask.Status,newTask.Priority, newTask.CreatedBy, newTask.CreatedAt, newTask.DueDate, id)
	if err != nil {
		return nil, err
	}
	tasks, err := taskManager.scanTasks(rows)
	if err != nil {
		return nil, err
	}
	return tasks[0], nil

}
func (taskManager *TaskManger)scanTasks(rows *sql.Rows) ([]*pb.Task, error) {
	var tasks[] *pb.Task
	for rows.Next(){
		var ID int
		var Name,Status,Priority, CreatedBy string
		var createdat, dueat time.Time
		err := rows.Scan(&ID, &Name,&Status,&Priority, &CreatedBy,&createdat,&dueat)
		if err != nil{
			return nil, err
		}
		time1,err := ptypes.TimestampProto(createdat)
		if err != nil{
			return nil, err
		}
		time2,err := ptypes.TimestampProto(dueat)
		if err != nil{
			return nil, err
		}
		task :=pb.Task{
			Id: int32(ID),
			Name:      Name,
			Status:    Status,
			Priority:  Priority,
			CreatedBy: CreatedBy,
			CreatedAt: time1,
			DueDate:   time2,
		}
		tasks = append(tasks,&task)
	}
	return tasks,nil

}