package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"lion.com/task_list/pb"
	"log"
)


func main()  {
	conn, err := grpc.Dial(":9000", grpc.WithInsecure())
	if err != nil{
		log.Fatalln(err)
	}

	client := pb.NewTaskServiceClient(conn)


	listTask, err := client.ListTask(context.Background(), &pb.ListTaskReq{})
	fmt.Println(listTask.Tasks)
	var s int
	_, err = fmt.Scan(&s)
	if err != nil {
		return
	}
}

