package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"google.golang.org/grpc"
	"lion.com/task_list/pb"
	"log"
	"net"
)

func main()  {
	lis, err := net.Listen("tcp",":9000")
	if err != nil{
		log.Fatalln(err)
	}

	grpcServer := grpc.NewServer()
	db,err := ConnectDB()
	if err != nil{
		log.Fatalln(err)
	}
	s := Server{taskManager: TaskManger{
		db: db,
	}}

	pb.RegisterTaskServiceServer(grpcServer,&s)

	if err := grpcServer.Serve(lis); err!=nil{
		log.Fatalln(err)
	}
}

func ConnectDB()(*sql.DB, error)  {
	const (
		host = "localhost"
		port = 5432
		dbname = "golangtestdb"
		user = "postgres"
		password = "root"

	)
	pInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",host,port,user,password,dbname)
	db,err := sql.Open("postgres",pInfo)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	log.Println("Connected to db")
	return db,nil
}

type Server struct {
	taskManager TaskManger

}

func (s *Server)CreateTask(ctx context.Context, req *pb.Task)(*pb.TaskRes,error)  {
	fmt.Println((*(&req)).GetPriority())

	task, err := s.taskManager.Create(req)
	if err != nil {
		return nil, err
	}
	return &pb.TaskRes{
		Task: task,
	}, nil
}
func (s *Server)ReadTask(ctx context.Context,req *pb.TaskReq) (*pb.TaskRes, error)  {
	task, err := s.taskManager.GetById(req.GetId())
	if err != nil {
		return nil, err
	}
	return &pb.TaskRes{
		Task: task,
	}, nil
}
func (s *Server)UpdateTask(ctx context.Context,req *pb.UpdateTaskReq)(*pb.UpdateTaskRes, error)  {
	task, err := s.taskManager.Update(req.GetNewTask(), req.Id)
	if err != nil{
		return nil, err
	}
	return &pb.UpdateTaskRes{Task: task}, nil
}
func (s *Server)DeleteTask(ctx context.Context, req *pb.DeleteTaskReq)(*pb.DeleteTaskRes, error)  {
	b, err := s.taskManager.Delete(req.Task)
	if err != nil {
		return nil, err
	}
	return &pb.DeleteTaskRes{
		Success: b,
	}, nil
}
func (s *Server)ListTask( ctx context.Context, req *pb.ListTaskReq) (*pb.ListTaskRes, error)  {
	all, err := s.taskManager.GetAll()
	if err != nil {
		return nil, err
	}

	return &pb.ListTaskRes{
		Tasks: all,
	}, nil
}